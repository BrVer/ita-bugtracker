class Project < ActiveRecord::Base
  attr_accessible :codename, :deadline, :image, :name, :id
  validates :name, :presence => true,
            :length => { :minimum => 3,:maximum => 60,
                         :too_long => "%{count} characters is the maximum allowed" }
  validates :codename, :presence => true,
            :length => { :minimum => 3,:maximum => 60,
                         :too_long => "%{count} characters is the maximum allowed" }

  has_many :user_projects
  has_many :users, :through => :user_projects

  has_many :versions
  has_many :components
  has_many :issues
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>", :small => "50x50>" }
end
