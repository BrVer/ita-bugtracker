class Like < ActiveRecord::Base
  attr_accessible :user_id, :issue_id
  belongs_to :user
  belongs_to :issue
end
