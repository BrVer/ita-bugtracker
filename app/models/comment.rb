class Comment < ActiveRecord::Base
  attr_accessible :message, :user_id, :issue_id
  validates :message, :presence => :true,:length => { :maximum => 300, :too_long => "%{count} characters is the maximum allowed" }
  belongs_to :issue
  belongs_to :user
end
