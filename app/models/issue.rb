class Issue < ActiveRecord::Base
  attr_accessible :description, :priority, :title, :issue_type, :status, :done, :author_id, :assignee_id, :project_id, :estimated, :deadline

  validates :done, :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 , :less_than_or_equal_to => 100 }
  validates :description, :presence => :true,:length => {:minimum => 5, :maximum => 1000, :too_long => "%{count} characters is the maximum allowed" }
  validates :title, :presence => :true,:length => {:minimum => 5, :maximum => 60, :too_long => "%{count} characters is the maximum allowed" }
  validates :issue_type, :presence => :true

  enum_attr :issue_type, %w(bug feature support)
  enum_attr :priority, %w(low normal high immediately)
  enum_attr :status, %w(new in_progress resolved closed)
  has_and_belongs_to_many :components
  has_and_belongs_to_many :versions
  has_many :histories
  belongs_to :author,
             :class_name => "User",
             :foreign_key => "author_id"

  belongs_to :assignee,
             :class_name => "User",
             :foreign_key => "assignee_id"

  belongs_to :project
  has_many :attached_files
  has_many :comments
  has_many :likes

  def paramsForHistory
    params = Hash.new
    params[:priority] = self.priority
    params[:status] = self.status
    params[:done] = self.done
    params[:assignee_id] = self.assignee_id
    return params
  end

  def customUpdate (params)
    if(self.status == :new and params[:status] == 'in_progress') then
      self.deadline = DateTime.now.advance(:hours => self.estimated.hour, :minutes =>self.estimated.min)
    end
    if((self.status == :new or self.status == :in_progress) and (params[:status] == 'resolved' or params[:status] == 'closed')) then
      self.done = 100;
    else
      self.done = params[:done]
    end
    self.priority = params[:priority]
    self.status = params[:status]
    self.assignee_id = params[:assignee_id]
    return self
  end

  scope :created_after, ->(time) { where("created_at > ?", time) }
  scope :where_type, ->(type) { where("issue_type = ?", type) }
  scope :where_status, ->(status) { where("status = ?", status) }
  scope :where_assignee, ->(assignee_id) { where("assignee_id = ?", assignee_id) }
  scope :where_author, ->(author_id) { where("author_id = ?", author_id) }
  scope :where_priority, ->(priority) { where("priority = ?", priority) }

end
