class History < ActiveRecord::Base
  attr_accessible :message, :priority, :status, :done, :assignee_id, :author_id, :issue_id
  validates :done, :numericality => { :only_integer => true ,
      :greater_than_or_equal_to => 0 , :less_than_or_equal_to => 100}
  validates :message, :length => { :maximum => 300, :too_long => "%{count} characters is the maximum allowed" }

  belongs_to :issue

  belongs_to :author,
             :class_name => "User",
             :foreign_key => "author_id"

  belongs_to :assignee,
             :class_name => "User",
             :foreign_key => "assignee_id"

  enum_attr :priority, %w(low ^normal high immediately)
  enum_attr :status, %w(^new in_progress resolved closed)
end
