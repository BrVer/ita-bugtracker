class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end

  def add_avatar
    @user = User.find(current_user.id).update_column(:avatar, params[:users][:avatar])
  end
end
