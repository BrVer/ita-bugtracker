class ComponentsController < ApplicationController
  def create
    @project = Project.find(params[:components][:project_id])
    if !(Component.where(:name => params[:components][:name], :project_id => @project.id).exists?)
      @component = Component.new(params[:components])
      if @component.save
        respond_to do |format|
          format.json { render :json => @component }
          format.js #added
        end
      end
    end
  end

  def delete
    @component = Component.find(params[:id])
    @component.destroy
    respond_to do |format|
      format.json { head :no_content }
      format.js #added
    end
  end
end
