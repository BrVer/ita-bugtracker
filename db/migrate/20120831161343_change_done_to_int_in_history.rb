class ChangeDoneToIntInHistory < ActiveRecord::Migration
  def up
    change_table :histories do |t|
      t.remove :done
      t.integer :done
    end
  end

  def down
  end
end
