class RenamingIssueType < ActiveRecord::Migration
  def up
    change_table :issues do |t|
      t.rename :type, :issue_type
    end
  end

  def down
  end
end
