class AddFkToUserProjects < ActiveRecord::Migration
  def change
    add_foreign_key(:user_projects, :users, dependent: :delete)
    add_foreign_key(:user_projects, :projects, dependent: :delete)
  end
end
