class RemoveColumnImageFromProjectsAndUsers < ActiveRecord::Migration
  def change
    remove_column(:users, :image)
    remove_column(:projects, :image)
  end
end
