class AddDeadlineAndEstimatedTimeToIssue < ActiveRecord::Migration
  def change
    change_table :issues do |t|
      t.time :estimated
      t.datetime :deadline
      end
  end
end
