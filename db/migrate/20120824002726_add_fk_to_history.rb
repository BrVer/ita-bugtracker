class AddFkToHistory < ActiveRecord::Migration
  def change
    add_foreign_key(:histories, :users)
    add_foreign_key(:histories, :issues)
  end
end
