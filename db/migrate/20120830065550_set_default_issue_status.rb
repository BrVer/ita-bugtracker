class SetDefaultIssueStatus < ActiveRecord::Migration
  def up
    change_column_default(:issues, :done, 0)
  end

  def down
    change_column_default(:issues, :done, NULL)
  end
end
