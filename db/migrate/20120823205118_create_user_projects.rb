class CreateUserProjects < ActiveRecord::Migration
  def change
    create_table :user_projects do |t|
      t.enum :role
      t.references :user
      t.references :project

      t.timestamps
    end
  end
end
